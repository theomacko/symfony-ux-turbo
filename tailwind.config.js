module.exports = {
	content: ['./templates/**/*.html.twig', './assets/**/*.js'],
	theme: {
		colors: {
			grey: {
				light: '#e0e0e0',
				medium: {
					light: '#9e9e9e',
					dark: '#424242'
				},
				dark: '#212121'
			},
			green: '#00897B',
			purple: '#673AB7',
			red: '#C62828',
			white: '#ffffff',
			black: '#000000'
		},
		fontFamily: {
			rude: ['Rude', 'sans-serif'],
			archia: ['Archia', 'sans-serif']
		},
		extend: {
			height: {
				header: '8.2rem',
				btnIconSm: '2rem'
			},
			width: {
				btnIconSm: '2rem'
			},
			minHeight: {
				sm: '24rem',
				md: '36rem',
				lg: '58rem'
			},
			maxWidth: {
				sm: '56.5rem',
				md: '76.6rem',
				lg: '92.4rem',
				xl: '102.4rem'
			},
			spacing: {
				header: '8.2rem'
			},
			zIndex: {
				header: '1000',
				overlay: '2000',
				modal: '3000'
			}
		}
	},
	plugins: [require('@tailwindcss/forms')]
}
