import { Controller } from '@hotwired/stimulus'
import Swal from 'sweetalert2'

import 'sweetalert2/dist/sweetalert2.min.css'

export default class extends Controller {
	connect() {
		Swal.fire({
			toast: true,
			html: this.element.innerHTML,
			customClass: {
				popup: `notification notification--${this.element.dataset.type}`,
				htmlContainer: 'notification-text'
			},
			position: 'bottom-right',
			showConfirmButton: false,
			timer: 3000,
			didOpen: toast => {
				toast.addEventListener('mouseenter', Swal.stopTimer)
				toast.addEventListener('mouseleave', Swal.resumeTimer)
				toast.addEventListener('click', Swal.close)
			}
		})
	}
}
