import MicroModal from 'micromodal'
import { Controller } from '@hotwired/stimulus'

export default class extends Controller {
	static targets = ['modalContainer']

	initialize() {
		this.config = {
			isOpenClass: 'is-open',
			modalId: 'main-modal'
		}
	}

	connect() {
		MicroModal.init(this.modalConfig)
	}

	openModal(event) {
		event.preventDefault()

		MicroModal.show(this.config.modalId, this.modalConfig)

		const self = this

		fetch(event.target.dataset.micromodalPath, {
			headers: {
				'X-Requested-With': 'XMLHttpRequest'
			}
		})
			.then(res => res.text())
			.then(html => {
				if (this.hasModalContainerTarget)
					self.modalContainerTarget.innerHTML = html
			})
			.catch(err => console.log(err))
	}

	get modalConfig() {
		return {
			onClose: modal => {
				setTimeout(() => {
					this.modalContainerTarget.innerHTML = ''
				}, 500)
			},
			disableScroll: true,
			awaitOpenAnimation: true,
			awaitCloseAnimation: true
		}
	}
}
