import flatpickr from 'flatpickr'
import { Controller } from '@hotwired/stimulus'

import 'flatpickr/dist/flatpickr.min.css'

export default class extends Controller {
  connect() {
    this.datepicker = flatpickr(this.element, this.config)
  }

  get config() {
    const date = new Date()
    date.setMonth(date.getMonth() - 1)

    return {
      dateFormat: 'Y-m-d',
      altFormat: 'd-m-Y',
      altInput: true,
      defaultDate:
        this.element.value ??
        `${date.getFullYear()} ${date.getMonth()} ${date.getDay()}`
    }
  }
}
