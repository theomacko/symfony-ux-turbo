import { Controller } from '@hotwired/stimulus'
import * as FilePond from 'filepond'
import 'filepond/dist/filepond.min.css'

export default class extends Controller {
  connect() {
    this.fileUploader = FilePond.create(this.element, this.config)
  }

  get config() {
    return {
      storeAsFile: true
    }
  }
}
