import { Controller } from '@hotwired/stimulus'
import Choices from 'choices.js'

import 'choices.js/public/assets/styles/choices.min.css'

export default class extends Controller {
  static values = {
    multiple: Boolean
  }

  connect() {
    this.select = new Choices(this.element, this.config)
  }

  get config() {
    const defaultSettings = {
      allowHTML: true,
      searchEnabled: false,
      searchChoices: false,
      classNames: {
        item: 'choices__item text-grey-medium-dark'
      }
    }

    return this.multipleValue
      ? {
          ...defaultSettings,
          removeItemButton: true,
          maxItemCount: 4
        }
      : defaultSettings
  }
}
