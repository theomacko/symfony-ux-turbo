<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211109153000 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE actor_translation ADD actor_id INT NOT NULL');
        $this->addSql('ALTER TABLE actor_translation ADD CONSTRAINT FK_30E528A410DAF24A FOREIGN KEY (actor_id) REFERENCES actor (id)');
        $this->addSql('CREATE INDEX IDX_30E528A410DAF24A ON actor_translation (actor_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE actor_translation DROP FOREIGN KEY FK_30E528A410DAF24A');
        $this->addSql('DROP INDEX IDX_30E528A410DAF24A ON actor_translation');
        $this->addSql('ALTER TABLE actor_translation DROP actor_id');
    }
}
