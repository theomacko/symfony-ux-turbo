<?php

namespace App\Form;

use App\Entity\Actor;
use App\Entity\Category;
use App\Entity\Movie;
use App\Services\CoreService;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EditMovieType extends AbstractType
{
    private $coreService;

    public function __construct(CoreService $coreService)
    {
        $this->coreService = $coreService;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('status', ChoiceType::class, [
                'choices' => [
                    'En attente' => 'pending',
                    'En ligne'   => 'enabled'
                ]
            ])
            ->add('releaseDate', DateType::class, [
                'widget' => 'single_text'
            ])
            ->add('translations', CollectionType::class, [
                'entry_type' => NewMovieType::class,
                'required'   => false
            ])
            ->add('categories', EntityType::class, [
                'class'        => Category::class,
                'choice_label' => function ($category) {
                    return $this->coreService->coreI18n($category, 'name');
                },
                'multiple' => true
            ])
            ->add('actors', EntityType::class, [
                'class'        => Actor::class,
                'multiple'     => true,
                'choice_label' => function ($category) {
                    return $this->coreService->coreI18n($category, 'firstname') . ' ' . $this->coreService->coreI18n($category, 'lastname');
                },
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Movie::class,
        ]);
    }
}
