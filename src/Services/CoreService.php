<?php

namespace App\Services;

class CoreService
{
    public function coreI18n($entity, string $field = 'title', string $locale = 'fr')
    {
        if (!$entity || empty($entity->getTranslations())) {
            return '';
        }

        foreach ($entity->getTranslations() as $translation) {
            if ($translation->getLocale() == $locale) {
                return $translation->{'get' . $field}();
            }
        }

        return '';
    }
}
