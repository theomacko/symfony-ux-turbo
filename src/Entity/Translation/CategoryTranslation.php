<?php

namespace App\Entity\Translation;

use App\Entity\Category;
use App\Entity\Trait\TimestampTrait;
use Doctrine\ORM\Mapping as ORM;
use Faker\Factory;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use function Symfony\Component\String\u;

/**
 * @ORM\Entity()
 * @UniqueEntity(fields = {"name", "locale"}, message ="An entity with this title already exists", errorPath = "name")
 * @ORM\HasLifecycleCallbacks()
 */
class CategoryTranslation
{
    use TimestampTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $identifier;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $locale;

    /**
     * @ORM\ManyToOne(targetEntity=Category::class, inversedBy="translations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $category;

    public function __construct(string $locale = 'fr')
    {
        $this->setLocale($locale);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getIdentifier(): ?string
    {
        return $this->identifier;
    }

    public function setIdentifier(string $identifier): self
    {
        $this->identifier = $identifier;

        return $this;
    }

    public function getLocale(): ?string
    {
        return $this->locale;
    }

    public function setLocale(string $locale): self
    {
        $this->locale = $locale;

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updateIdentifier(): void
    {
        if (!$name = $this->getName()) {
            $faker = Factory::create();
            $name  = $faker->name;
        }

        $this->setIdentifier(u($name)->snake());
    }
}
