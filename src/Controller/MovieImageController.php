<?php

namespace App\Controller;

use App\Entity\Image;
use App\Form\AddMovieImageType;
use App\Repository\MovieRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/admin/movie/{movieId}/image')]
class MovieImageController extends AbstractController
{
    const PATH_PREFIX   = 'app_admin_movie_image_';
    const PATH_TEMPLATE = 'admin/movie/image/';

    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    #[Route('/add', name: self::PATH_PREFIX . 'add')]
    public function index(Request $request, MovieRepository $movieRepository, int $movieId): Response
    {
        if (!$movieId) {
            $this->addFlash('error', 'No "movieId" provided in the url');

            return $this->redirectToRoute('app_admin_movie_index');
        }

        $imageWasAdded = false;
        $image         = new Image();
        $form          = $this->createForm(AddMovieImageType::class, $image);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $movie = $movieRepository->find($movieId);

            if (!$movie) {
                $this->addFlash('error', 'No movie found with the id : ' . $movieId);

                return $this->redirectToRoute('app_admin_movie_index');
            }

            $this->em->persist($image);
            $this->em->flush();

            $imageWasAdded = true;
        }

        return $this->render(self::PATH_TEMPLATE . '/_add.html.twig', [
            'form'          => $form->createView(),
            'imageWasAdded' => $imageWasAdded
        ]);
    }
}
