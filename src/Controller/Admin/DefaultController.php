<?php

namespace App\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    #[Route('/admin', name: 'app_admin_default_index')]
    public function index(): Response
    {
        return $this->render('admin/default/index.html.twig', [
            'title'      => 'Admin - home',
            'page_title' => 'Home'
        ]);
    }
}
