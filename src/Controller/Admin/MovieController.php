<?php

namespace App\Controller\Admin;

use App\Entity\Movie;
use App\Entity\Translation\MovieTranslation;
use App\Form\EditMovieType;
use App\Form\NewMovieType;
use App\Repository\MovieRepository;
use App\Services\CoreService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/admin/movie')]
class MovieController extends AbstractController
{
    const PATH_PREFIX   = 'app_admin_movie_';
    const PATH_TEMPLATE = 'admin/movie/';

    private $em;
    private $coreService;
    private $translator;

    public function __construct(EntityManagerInterface $em, CoreService $coreService, TranslatorInterface $translator)
    {
        $this->em          = $em;
        $this->coreService = $coreService;
        $this->translator  = $translator;
    }

    #[Route('', name: self::PATH_PREFIX . 'index')]
    public function index(MovieRepository $movieRepository): Response
    {
        return $this->render(self::PATH_TEMPLATE . 'index.html.twig', [
            'title'      => 'Admin - movie',
            'page_title' => 'Movies',
            'movies'     => $movieRepository->findAll()
        ]);
    }

    #[Route('/new', name: self::PATH_PREFIX . 'new')]
    public function new(Request $request, MovieRepository $movieRepository): Response
    {
        $locale           = $request->getLocale();
        $movieTranslation = new MovieTranslation($locale);
        $form             = $this->createForm(NewMovieType::class, $movieTranslation);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $movie = new Movie();
            $movieTranslation->setMovie($movie);

            $this->em->persist($movie);
            $this->em->persist($movieTranslation);
            $this->em->flush();

            if (!$movieRepository->findMovieByLocale('en', $movie->getId())) {
                $movieTranslationEN = new MovieTranslation('en');

                $movieTranslationEN
                    ->setMovie($movie)
                    ->setTitle($movieTranslation->getTitle() . ' - en')
                ;

                $this->em->persist($movieTranslationEN);
                $this->em->flush();
            }

            $this->addFlash('success', 'New movie created');

            return $this->redirectToRoute(self::PATH_PREFIX . 'index');
        }

        return $this->renderForm(self::PATH_TEMPLATE . 'new.html.twig', [
            'title'      => 'Admin - movie | new',
            'page_title' => 'new movie',
            'form'       => $form,
        ]);
    }

    #[Route('/edit/{id}', name: self::PATH_PREFIX . 'edit')]
    public function edit(Request $request, Movie $movie): Response
    {
        $form = $this->createForm(EditMovieType::class, $movie);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();

            $this->addFlash('success', 'The movie ' . $this->coreService->coreI18n($movie) . ' was updated');
        }

        return $this->renderForm(self::PATH_TEMPLATE . 'edit.html.twig', [
            'title'      => 'Admin - movie | edit',
            'page_title' => 'editer le film',
            'form'       => $form
        ]);
    }

    #[Route('/delete/{id}', name: self::PATH_PREFIX . 'delete', methods: 'DELETE')]
    public function delete(Request $request, Movie $movie): Response
    {
        if ($token = $request->request->get('_csrf_token')) {
            if ($this->isCsrfTokenValid('delete-movie-' . $movie->getId(), $token)) {
                $title = $this->coreService->coreI18n($movie);

                $this->em->remove($movie);
                $this->em->flush();

                $this->addFlash('success', 'The movie ' . $title . ' was deleted');
            } else {
                $this->addFlash('error', 'The token is not valid');
            }
        } else {
            $this->addFlash('error', 'The token is missing');
        }

        return $this->redirectToRoute(self::PATH_PREFIX . 'index');
    }
}
