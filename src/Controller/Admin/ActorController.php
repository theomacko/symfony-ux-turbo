<?php

namespace App\Controller\Admin;

use App\Entity\Actor;
use App\Entity\Translation\ActorTranslation;
use App\Form\EditActorType;
use App\Form\NewActorType;
use App\Repository\ActorRepository;
use App\Services\CoreService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/admin/actor')]
class ActorController extends AbstractController
{
    const PATH_PREFIX   = 'app_admin_actor_';
    const PATH_TEMPLATE = 'admin/actor/';

    private $em;
    private $coreService;
    private $translator;

    public function __construct(EntityManagerInterface $em, CoreService $coreService, TranslatorInterface $translator)
    {
        $this->em          = $em;
        $this->coreService = $coreService;
        $this->translator  = $translator;
    }

    #[Route('', name: 'app_admin_actor_index')]
    public function index(ActorRepository $actorRepository): Response
    {
        return $this->render(self::PATH_TEMPLATE . 'index.html.twig', [
            'title'      => 'Admin - actors',
            'page_title' => 'Actors',
            'actors'     => $actorRepository->findAll()
        ]);
    }

    #[Route('/new', name: self::PATH_PREFIX . 'new')]
    public function new(Request $request, ActorRepository $actorRepository): Response
    {
        $actorTranslation = new ActorTranslation();
        $form             = $this->createForm(NewActorType::class, $actorTranslation);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $actor = new Actor();
            $actorTranslation->setActor($actor);

            $this->em->persist($actor);
            $this->em->persist($actorTranslation);
            $this->em->flush();

            if (!$actorRepository->findActorByLocale('en', $actor->getId())) {
                $actorTranslationEN = new ActorTranslation('en');

                $actorTranslationEN
                    ->setActor($actor)
                    ->setFirstname($actorTranslation->getFirstname())
                    ->setLastname($actorTranslation->getLastname())
                    ->setNationality($form->getData()->getNationality())
                ;

                $this->em->persist($actorTranslationEN);
                $this->em->flush();
            }

            $this->addFlash('success', 'New actor created');

            return $this->redirectToRoute(self::PATH_PREFIX . 'index');
        }

        return $this->renderForm(self::PATH_TEMPLATE . 'new.html.twig', [
            'title'      => 'Admin - actors | new',
            'page_title' => 'New Actor',
            'form'       => $form
        ]);
    }

    #[Route('/edit/{id}', name: self::PATH_PREFIX . 'edit')]
    public function edit(Request $request, Actor $actor): Response
    {
        $form = $this->createForm(EditActorType::class, $actor);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();

            $this->addFlash('success', 'The actor ' . $this->coreService->coreI18n($actor, 'firstname') . ' ' . $this->coreService->coreI18n($actor, 'lastname') . ' was edited');

            return $this->redirectToRoute('app_admin_actor_index');
        }

        return $this->renderForm(self::PATH_TEMPLATE . 'edit.html.twig', [
            'title'      => 'Admin - actor | edit',
            'page_title' => 'Edit ' . $this->coreService->coreI18n($actor, 'firstname') . ' ' . $this->coreService->coreI18n($actor, 'lastname'),
            'form'       => $form
        ]);
    }

    #[Route('/delete/{id}', name: self::PATH_PREFIX . 'delete', methods: 'DELETE')]
    public function delete(Request $request, Actor $actor): Response
    {
        if ($token = $request->request->get('_csrf_token')) {
            if ($this->isCsrfTokenValid('delete-actor-' . $actor->getId(), $token)) {
                $name = $this->coreService->coreI18n($actor, 'firstname') . ' ' . $this->coreService->coreI18n($actor, 'lastname');

                $this->em->remove($actor);
                $this->em->flush();

                $this->addFlash('success', 'The actor ' . $name . ' was deleted');
            } else {
                $this->addFlash('error', 'The token is not valid');
            }
        } else {
            $this->addFlash('error', 'The token is missing');
        }

        return $this->redirectToRoute(self::PATH_PREFIX . 'index');
    }
}
