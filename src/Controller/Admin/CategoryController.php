<?php

namespace App\Controller\Admin;

use App\Entity\Category;
use App\Entity\Translation\CategoryTranslation;
use App\Form\EditCategoryType;
use App\Form\NewCategoryType;
use App\Repository\CategoryRepository;
use App\Services\CoreService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/admin/category')]
class CategoryController extends AbstractController
{
    const PATH_PREFIX   = 'app_admin_category_';
    const PATH_TEMPLATE = 'admin/category/';

    private $em;
    private $coreService;
    private $translator;

    public function __construct(EntityManagerInterface $em, CoreService $coreService, TranslatorInterface $translator)
    {
        $this->em          = $em;
        $this->coreService = $coreService;
        $this->translator  = $translator;
    }

    #[Route('', name: self::PATH_PREFIX . 'index')]
    public function index(CategoryRepository $categoryRepository): Response
    {
        $categories = $categoryRepository->findAll();

        return $this->render(self::PATH_TEMPLATE . 'index.html.twig', [
            'title'      => 'Admin - category',
            'page_title' => 'Categorie',
            'categories' => $categories
        ]);
    }

    #[Route('/new', name: self::PATH_PREFIX . 'new')]
    public function new(Request $request, CategoryRepository $categoryRepository): Response
    {
        $categoryTranslation = new CategoryTranslation();
        $form                = $this->createForm(NewCategoryType::class, $categoryTranslation);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $category = new Category();
            $categoryTranslation->setCategory($category);

            $this->em->persist($category);
            $this->em->persist($categoryTranslation);
            $this->em->flush();

            if (!$categoryRepository->findCategoryByLocale('en', $category->getId())) {
                $categoryTranslationEN = new CategoryTranslation('en');

                $categoryTranslationEN->setCategory($category);
                $categoryTranslationEN->setName($categoryTranslation->getName() . ' - en');

                $this->em->persist($categoryTranslationEN);
                $this->em->flush();
            }

            $this->addFlash('success', 'New category created');

            return $this->redirectToRoute(self::PATH_PREFIX . 'index');
        }

        return $this->renderForm(self::PATH_TEMPLATE . 'new.html.twig', [
            'title'      => 'Admin - category | new',
            'page_title' => 'New category',
            'form'       => $form
        ]);
    }

    #[Route('/edit/{id}', name: self::PATH_PREFIX . 'edit')]
    public function edit(Request $request, Category $category): Response
    {
        $form = $this->createForm(EditCategoryType::class, $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();

            $this->addFlash('success', 'The category ' . $this->coreService->coreI18n($category, 'name') . ' was edited');
        }

        return $this->renderForm(self::PATH_TEMPLATE . 'edit.html.twig', [
            'title'      => 'Admin - category | edit',
            'page_title' => 'Edit ' . $this->coreService->coreI18n($category, 'name'),
            'form'       => $form
        ]);
    }

    #[Route('/delete/{id}', name: self::PATH_PREFIX . 'delete', methods: 'DELETE')]
    public function delete(Request $request, Category $category): Response
    {
        if ($token = $request->request->get('_csrf_token')) {
            if ($this->isCsrfTokenValid('delete-category-' . $category->getId(), $token)) {
                $name = $this->coreService->coreI18n($category, 'name');

                $this->em->remove($category);
                $this->em->flush();

                $this->addFlash('success', 'The category ' . $name . ' was deleted');
            } else {
                $this->addFlash('error', 'The token is not valid');
            }
        } else {
            $this->addFlash('error', 'The token is missing');
        }

        return $this->redirectToRoute(self::PATH_PREFIX . 'index');
    }
}
